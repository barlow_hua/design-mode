package com.barlowhua.singleton;


import java.io.Serializable;

/**
 * 懒汉式
 * 优点：简单可靠，推荐使用； 由JVM保证只加载一个实列
 * 缺点：’反射、反序列化‘可以破坏单例的限制
 * 优化
 *  反射： 可以加入静态变量记录实列个数，如果大于1则报错；
 *  序列化： 加入readResolve()方法，指定反序列化时候创建对象的方式；
 * @author barlow
 */
public class Singleton01 implements Serializable {

    /**
     * 私有化构造函数，禁止代码中直接new出实列
     */
    private Singleton01(){}


    private static final Singleton01 SINGLETON;

    static {
        SINGLETON = new Singleton01();
    }


    public static Singleton01 getSingleton() {
        return SINGLETON;
    }

    /**
     * 该方法非必要， 正常的单例一般不会实现Serializable接口，所有可以不用管
     * 如果单例类实现了Serializable接口，可以通过写入readResolve()方法来避免反序列化时候破坏单例，
     * 因为反序列化时候会检查，如果对象有该方法则invoke反射调用类的readResolve方法生产对象。
     * @return 单例对象
     */
    private Object readResolve() {
        return SINGLETON;
    }

}
