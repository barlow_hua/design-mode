package com.barlowhua.singleton;

/**
 * 策略模式常规的四种写法
 * Singleton01: 懒汉式
 * Singleton02： DCL模式
 * Singleton03： 静态内部类
 * Singleton04： 枚举单例
 *
 * 枚举单例是目前最完美的方式，天然避免的反射、序列化原因照成的破坏；
 * 其它几种方式如果不特殊处理，都有反射，序列化破坏单例的风险。
 * @author barlow.hua
 */
