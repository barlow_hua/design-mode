package com.barlowhua.singleton;

/**
 * DCL单例
 * 优点：实现懒加载；
 * 缺点：反射、序列化可以破坏单例
 * @author barlow
 * @date 2021/7/10
 */
public class Singleton02 {

    private Singleton02() {}

    /**
     * 必须添加volatile: 避免jvm在JIT时候的指令重排
     */
    private static volatile Singleton02 SINGLETON;

    public static Singleton02 getSingleton() {
        if (SINGLETON == null) {
            synchronized (Singleton02.class) {
                if (SINGLETON == null) {
                    SINGLETON = new Singleton02();
                }
            }
        }
        return SINGLETON;
    }

}
