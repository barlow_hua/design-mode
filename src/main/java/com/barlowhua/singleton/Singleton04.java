package com.barlowhua.singleton;

/**
 * 枚举单例
 * jvm的枚举禁止反射
 * @author barlow
 * @date 2021/7/10
 */
public enum Singleton04 {

    /**
     * 单例对象
     */
    INSTANCE;

    private final Object object;

    Singleton04() {
        //初始化单例资源
        object = new Object();
    }

    public Object getSingleResource() {
        return object;
    }


}
