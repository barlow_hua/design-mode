package com.barlowhua.singleton;

/**
 * 静态内部类模式
 * jvm规则，内部类默认不会被加载，只有首次被使用的时候才会加载，
 * 因为是“类”有jvm保证只会加载一次。
 * @author barlow
 * @date 2021/7/10
 */
public class Singleton03 {

    private Singleton03(){}

    public static Singleton03 getSingleton() {
        return SingletonHolder.SINGLETON;
    }

    private static class SingletonHolder {
        private static final Singleton03 SINGLETON;
        static {
            SINGLETON = new Singleton03();
        }
    }
}
