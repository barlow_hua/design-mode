package com.barlowhua.singleton;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author barlow
 * @date 2021/7/10
 */
public class SingletonTest {

    private static final int COUNT = 20;


    /**
     * 懒汉式验证
     */
    @Test
    public void test01() throws Exception {
        //两次对象的内存地址一致，说明是同一个对象
        System.out.println(Singleton01.getSingleton() == Singleton01.getSingleton());
        // 反射 - 破坏单例
        Constructor<Singleton01> constructor = Singleton01.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        //反射每次获取的对象都不是同一个
        System.out.println(Singleton01.getSingleton() == constructor.newInstance());
        System.out.println(constructor.newInstance() == constructor.newInstance());

        //readResolve方法规避反序列化破坏单例
        byte[] serialize = SerializationUtils.serialize(Singleton01.getSingleton());
        Singleton01 singleton01 = SerializationUtils.deserialize(serialize);
        //如果单例类没有readResolve()方法就会破坏单例，
        System.out.println(singleton01 == Singleton01.getSingleton());
    }


    /**
     * DCL单例验证
     */
    @Test
    public void test02() {
        ExecutorService service = Executors.newFixedThreadPool(COUNT);
        for (int i = 0; i < COUNT; i++) {
            //多线程情况下打印单例对象内存地址，如果全部一样说明可靠
            service.submit(() -> System.out.println(Singleton02.getSingleton()));
        }
        service.shutdown();
    }

    /**
     * 静态内部类验证
     */
    @Test
    public void test03() {
        ExecutorService service = Executors.newFixedThreadPool(COUNT);
        for (int i = 0; i < COUNT; i++) {
            service.submit(() -> System.out.println(Singleton03.getSingleton()));
        }
        service.shutdown();
    }


    @Test
    public void test04() {
        ExecutorService service = Executors.newFixedThreadPool(COUNT);
        for (int i = 0; i < COUNT; i++) {
            service.submit(() -> System.out.println(Singleton04.INSTANCE.getSingleResource()));
        }
        service.shutdown();
    }

}
