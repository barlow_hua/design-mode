# design-mode

#### 介绍
Java版本设计模式

#### 学习说明
按目录划分要学习的设计模式
package-info.java： 模式结束
test目录下有对应的测试用例

1.  singleton 单例模式
2.  strategy 策略模式
